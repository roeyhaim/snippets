<?php

/**
 * Generate strong password
 * @param int $length Password length
 * @param string $sets l - lowercase<br>u - uppercase<br>n - numbers<br>s - special chars
 * @return string
 */
function password($length = 8, $sets = 'luns')
{
    $sets = strtolower($sets);
    $lenght = (int) $lenght;
    $l = "abcdefghijklmnopqrstuvwxyz";
    $u = strtoupper($l);
    $n = "0123456789";
    $s = ".-+=_,!@$#*%[]{}&~^()";
    $chars = '';

    foreach (str_split($sets) as $p) {
        $chars .= $$p;
    }

    $len = strlen($chars) - 1;
    $pw = '';
    
    for ($i = 0; $i < $length; $i++) {
        $pw .= substr($chars, rand(0, $len), 1);
    }

    return str_shuffle($pw);
}
