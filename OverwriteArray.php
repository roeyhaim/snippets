<?php
/**
 * Overwrites an array with values from input arrays.
 * Keys that do not exist in the first array will not be added!
 * 
 * @param array $array1 Master array
 * @param array $array2 Input arrays that will overwrite existing values
 * @return array
 */
function overwrite(array $array1, array $array2)
{
    foreach (array_intersect_key($array2, $array1) as $key => $value) {
        $array1[$key] = $value;
    }

    return $array1;
}
