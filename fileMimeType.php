<?php
/**
 * Return the real file type
 * @param string $file File location
 * @return string file_type
 */
function fileMimeType($file)
{
    if(!is_file($file)) {
        throw new \InvalidArgumentException('File "'.$file.'" not exist');
    }
    
    $type = exif_imagetype($file);
    if($type && isset($type['mime'])) {
        return $type['mime'];
    }
    
    if(class_exists('finfo', FALSE)) {
        $finfo = new \finfo(FILEINFO_MIME_TYPE);
        return $finfo->file($file);
    }
    
    if(function_exists('mime_content_type')) {
        return mime_content_type($file);
    }
    
    throw new \Exception('Please enable FINFO for mime detection');
}
