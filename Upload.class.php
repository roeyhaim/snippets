<?php
class Upload
{
    /**
     * @var string $default_directory The default directory for uploading the file
     */
  	public static $default_directory = 'files';
  
    /**
     * Generate a unique filename
     * @param string $name The name of the original file
     * @return string
     */
  	public static function generateName($name)
  	{
  		$ext = pathinfo($name, PATHINFO_EXTENSION);
  		$name = uniqid(NULL, TRUE);
  		return $name . '.' . $ext;
  	}
  
    /**
     * Upload the file and return array with the new name, old name, full path of the new file
     * @param array $file file data array to upload
     * @param mixed $directory Where to save the file. if FALSE the file will be save at the default directory
     * @return array
     */
  	public static function save(array $file, $directory = FALSE)
  	{
  		if (!self::valid($file)) {
  			return FALSE;
  		}
  
  		$directory = $directory ? $directory : self::$default_directory;
  		$directory = trim($directory, DS);
  		$newfile = self::generateName($file['name']);
  
  		move_uploaded_file($file['tmp_name'], $directory . DS . $newfile);
  		
  		$newfile = array(
              'new_name' => $newfile, 
              'original' => $file['name'], 
              'full_path' => $directory . DS . $newfile 
  		);
  
  		return $newfile;
  	}
  
    /**
     * Tests if upload data is valid, even if no file was uploaded.
     * @param array $file $_FILES item
     * @return boolean
     */
  	public static function valid($file)
  	{
  		return isset($file['name']) 
  				&& isset($file['size']) 
  				&& isset($file['tmp_name']) 
  				&& isset($file['type']) 
  				&& isset($file['error']) 
  				&& $file['error'] == UPLOAD_ERR_OK 
  				&& is_uploaded_file($file['tmp_name']);
  	}
  
    /**
     * Test if an uploaded file is an allowed file type, by extension.
     * @param array $file $_FILES item
     * @param array $types Allowed file extensions
     * @return boolean
     */
  	public static function type(array $file, array $types)
  	{
  		$name = strtolower($file['name']);
  		$ext = pathinfo($name, PATHINFO_EXTENSION);
  		return in_array($ext, $types);
  	}

}
