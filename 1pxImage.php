<?php
    $img = imagecreate(1, 1);
    $color = imagecolorallocate($img, 0xFF, 0xFF, 0xFF);
    imagecolortransparent($img, $color);
    header("Content-type: image/png");
    imagepng($img);
    imagedestory($img);
?>
