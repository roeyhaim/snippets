<?php
/**
 * Flattent multidimensional array
 * @param array $array The source array
 * @return array Flatten_Array
 */
function flattenArray(array $array)
{
    $keys = array_keys($array);
    $is_assoc = array_keys($keys) !== $keys;

    $flat = array();
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $flat = array_merge($flat, flattenArray($value));
        } else {
            if ($is_assoc) {
                $flat[$key] = $value;
            } else {
                $flat[] = $value;
            }
        }
    }
    return $flat;
}
