<?php
/**
 * Return the values from a single column in the input array
 * 
 * @param array $array A multi-dimensional array (record set) from which to pull a column of values.
 * @param string $key The column of values to return. This value may be the integer key of the column you wish to retrieve, or it may be the string key name for an associative array.
 * @return array
 */
function arrayColumn(array $array, $key)
{
    $values = array();
    foreach ($array as $row) {
        if (isset($row[$key])) {
            $values[] = $row[$key];
        }
    }

    return $values;
}
